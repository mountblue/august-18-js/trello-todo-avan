let cardCounter = 0;

function loadingData() {
  return new Promise(((resolve, reject) => {
    fetch('https://api.trello.com/1/boards/5b8e1e4d9292eb246de0c78b/checklists?checkItem_fields=all&fields=all&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb', {
      method: 'GET',
    })
      .then(response => response.json())
      .then((data) => {
        const traversing = $('.container .lists');
        cardCounter = data.length;

        for (let i = 0; i < data.length; i++) {
          $('<div/>').attr('id', data[i].idCard).addClass(`card-${i + 1}`).appendTo(traversing);
          $('<ul/>').attr('id', data[i].id).addClass(`list-group group-${i + 1}`).appendTo(`.card-${i + 1}`);

          const groupName = `.group-${i + 1}`;

          for (let j = 0; j < data[i].checkItems.length; j++) {
            if (data[i].checkItems[j].state == 'incomplete') {
              $('<li/>').addClass('list-group-item').attr('id', data[i].checkItems[j].id).html(data[i].checkItems[j].name)
                .appendTo(groupName);
              $('<input/>', { type: 'checkbox', name: 'todolists' }).prependTo(`#${data[i].checkItems[j].id}`);
              $('<p/>').appendTo(`#${data[i].checkItems[j].id}`).html('X').addClass('del-icon');
            }
          }
        }
        $('<input />', { type: 'text', name: 'text-box', placeholder: 'Adding new List' }).addClass('inputfield').attr('id', 'inputTextbox').appendTo('.container .lists');
      }).then(() => {
        resolve();
      })
      .catch((error) => {
        reject(error);
        console.log('Request failed', error);
      });
  }));
}



function addingcheckItem(checkListId, itemName) {
  fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${itemName}&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
    method: 'POST',
  })
    .then(response => response.json())
    .then((data) => {
      console.log(data);
      $('<li/>').addClass('list-group-item').attr('id', data.id).html(itemName)
        .appendTo(`#${checkListId}`);
      $('<input/>', { type: 'checkbox', name: 'todolists' }).prependTo(`#${data.id}`);
      $('<p/>').appendTo(`#${data.id}`).html('X').addClass('del-icon');

      $(`#${data.id} input[type=checkbox]`).on('change', function () {
        const cardId = $(this).parent().parent().parent()
          .attr('id');
        const checkItemId = $(this).parent().attr('id');
        if (this.checked) {
          $.ajax(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=complete&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
            method: 'PUT',
          });
        } else {
          $.ajax(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=incomplete&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
            method: 'PUT',
          });
        }
      });

      $(`#${data.id} .del-icon`).on('click', function () {
        const checkListId = $(this).parent().parent().attr('id');
        const checkItemId = $(this).parent().attr('id');
        $.ajax(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
          method: 'DELETE',
          success: $(this).parent().remove(),
        });
      });
    })
    .catch((error) => {
      console.log('Request failed', error);
    });
}
function addingCheckList(cardId, itemName) {
  fetch(`https://api.trello.com/1/cards/${cardId}/checklists?name=checklist&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
    method: 'POST',
  })
    .then(response => response.json())
    .then((data) => {
      $('<ul/>').attr('id', data.id).addClass(`list-group group-${1}`).appendTo(`#${cardId}`);
      addingcheckItem(data.id, itemName);
    })
    .catch((error) => {
      console.log('Request failed', error);
    });
}

function addingCard(idList, cardName, itemName) {
  fetch(`https://api.trello.com/1/cards?idList=${idList}&name=${cardName}&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
    method: 'POST',
  })
    .then(response => response.json())
    .then((data) => {
      console.log('Request succeeded with JSON response', data);
      $('<div/>').attr('id', data.id).addClass(`card-${++cardCounter}`).prependTo('.container .lists');
      addingCheckList(data.id, itemName);
    })
    .catch((error) => {
      console.log('Request failed', error);
    });
}


function checkbox() {
  $('input[type=checkbox]').on('change', function () {
    console.log('Hello');
    const cardId = $(this).parent().parent().parent()
      .attr('id');
    const checkItemId = $(this).parent().attr('id');
    if (this.checked) {
      $.ajax(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=complete&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
        method: 'PUT',
      });
    } else {
      $.ajax(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=incomplete&key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
        method: 'PUT',

      });
    }
  });
}

async function Loader() {
  await loadingData();

  $('.container .del-icon').on('click', function () {
    const checkListId = $(this).parent().parent().attr('id');
    const checkItemId = $(this).parent().attr('id');
    $.ajax(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb`, {
      method: 'DELETE',
      success: $(this).parent().remove()
    });
  });

  checkbox();


  $('#inputTextbox').on('keypress', (key) => {
    const boardId = '5b8e1e4d9292eb246de0c78b';
    const secret = 'key=ada43f13da37f800858bdfb1f538a7eb&token=047b834583b0b91facdec457be4833cb95d10b655b6994183a2432c8e85f79bb';
    let itemName = '';
    let cardName = '';
    if (key.keyCode == 13) {
      let fieldValue = $('#inputTextbox').val();
      fieldValue = fieldValue.split(':');

      itemName = fieldValue[0];
      cardName = fieldValue[1];
      $('#inputTextbox').val('');

      fetch(`https://api.trello.com/1/boards/${boardId}/lists?${secret}`, {
        method: 'GET',
      })
        .then(response => response.json())
        .then((data) => {
          addingCard(data[0].id, cardName, itemName);
        })
        .catch((error) => {
          console.log('Request failed', error);
        });
    }
  });
}

$('document').ready(() => {
  Loader();
});

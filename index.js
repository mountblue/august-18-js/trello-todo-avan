const express = require('express');

const app = express();

const router = express.Router();

const bodyParser = require('body-parser');

const path = require('path');


const port = process.env.port || 5000;

// router.use((req, res, next) => {
//   console.log('Time:', new Date().toLocaleString());
//   console.log('%s %s', req.method, req.url);
//   next();
// });


app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', router);
app.listen(port);
